package com.apple.ms1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.apple.ms1.dto.EmployeeDto;
import com.apple.ms1.service.EmployeeService;

@RestController
public class EmployeeController {

	@Autowired
	@Qualifier(value = "employeeServiceImpl")
	private EmployeeService employeeService;

	@GetMapping("/employees")
	public ResponseEntity<EmployeeDto[]> getEmployees() {

		EmployeeDto[] employeeDtos = employeeService.getEmployees();

		return new ResponseEntity<>(employeeDtos, HttpStatus.OK);

	}

	@GetMapping("/employees/{employeeid}")
	public ResponseEntity<EmployeeDto> getPatricluarEmployee(@PathVariable("employeeid") int employeeId) {

		EmployeeDto employeeDto = employeeService.getPatricularEmployees(employeeId);

		return new ResponseEntity<>(employeeDto, HttpStatus.OK);

	}

	@PostMapping("/employees")
	public ResponseEntity createEmployee(@RequestBody EmployeeDto employeeDto) {

		employeeService.createEmployee(employeeDto);

		return new ResponseEntity(HttpStatus.CREATED);

	}

	@PutMapping("/employees/{employeeid}")
	public ResponseEntity<Void> particularEmployeePut(@PathVariable("employeeid") int employeeId,
			@RequestBody EmployeeDto employeeDto) {
		employeeService.particularEmployeePut(employeeId, employeeDto);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@PatchMapping("/employees/{employeeid}")
	public ResponseEntity<Void> particularEmployeePatch(@PathVariable("employeeid") int employeeId,
			@RequestBody EmployeeDto employeeDto) {
		employeeService.particularEmployeePatch(employeeId, employeeDto);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@DeleteMapping("/employees/{employeeid}")
	public ResponseEntity<Void> particularEmployeeDelete(@PathVariable("employeeid") int employeeId) {
		employeeService.particularEmployeeDelete(employeeId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
