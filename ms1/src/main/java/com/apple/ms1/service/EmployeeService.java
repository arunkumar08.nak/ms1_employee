package com.apple.ms1.service;

import com.apple.ms1.dto.EmployeeDto;


public interface EmployeeService {
	
	public EmployeeDto[] getEmployees();
	
	public EmployeeDto getPatricularEmployees(int employeeId);
	
	public void createEmployee(EmployeeDto employeeDto);
	
	public void particularEmployeePut(int studentId, EmployeeDto employeeDto);
	
	public void particularEmployeePatch(int studentId, EmployeeDto employeeDto);
	
	public void particularEmployeeDelete(int studentId);

}
