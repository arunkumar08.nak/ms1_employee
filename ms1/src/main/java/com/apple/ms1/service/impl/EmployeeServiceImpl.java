package com.apple.ms1.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.stereotype.Service;

import com.apple.ms1.dto.EmployeeDto;
import com.apple.ms1.service.EmployeeService;

@Service(value = "employeeServiceImpl")
public class EmployeeServiceImpl implements EmployeeService {

	@Override
	public EmployeeDto[] getEmployees() {

		EmployeeDto[] employeeDtos = new EmployeeDto[20];
		Connection connection = getConnection();
		Statement stmt;
		try {
			stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("select name, age, refno, email from employee");
			int i = 0;
			while (rs.next()) {

				String name = rs.getString(1);
				int age = rs.getInt(2);
				int refno = rs.getInt(3);
				String email = rs.getString(4);
				EmployeeDto employeeDto = new EmployeeDto(name, age, refno, email);
				employeeDtos[i++] = employeeDto;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return employeeDtos;
	}

	@Override
	public EmployeeDto getPatricularEmployees(int employeeId) {

		EmployeeDto employeeDto = null;
		Connection connection = getConnection();
		Statement stmt;
		try {
			stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("select name, age, refno, email from employee where id=" + employeeId);

			while (rs.next()) {

				String name = rs.getString(1);
				int age = rs.getInt(2);
				int refno = rs.getInt(3);
				String email = rs.getString(4);
				employeeDto = new EmployeeDto(name, age, refno, email);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return employeeDto;
	}

	@Override
	public void createEmployee(EmployeeDto employeeDto) {

		Connection connection = getConnection();
		PreparedStatement pStmt;
		try {
			pStmt = connection.prepareStatement("insert into employee (name,age,refno,email) values (?, ?, ?, ?)");
			pStmt.setString(1, employeeDto.getName());
			pStmt.setInt(2, employeeDto.getAge());
			pStmt.setInt(3, employeeDto.getRefno());
			pStmt.setString(4, employeeDto.getEmail());

			boolean insertStatus = pStmt.execute();
			// System.out.println("Insertion Status:"+insertStatus);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void particularEmployeePut(int employeeId, EmployeeDto employeeDto) {
		Connection connection = getConnection();
		PreparedStatement pStmt;
		try {
			pStmt = connection.prepareStatement("update employee set name=?,age=?,refno=?,email=? where id=?");
			pStmt.setString(1, employeeDto.getName());
			pStmt.setInt(2, employeeDto.getAge());
			pStmt.setInt(3, employeeDto.getRefno());
			pStmt.setString(4, employeeDto.getEmail());
			pStmt.setInt(5, employeeId);

			boolean insertStatus = pStmt.execute();
			// System.out.println("Insertion Status:"+insertStatus);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void particularEmployeePatch(int employeeId, EmployeeDto employeeDto) {
		Connection connection = getConnection();
		PreparedStatement pStmt;
		EmployeeDto employeeDtoFromDB = null;
		Statement stmt;
		try {
			stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("select name, age, refno, email from employee where id=" + employeeId);

			while (rs.next()) {

				String name = rs.getString(1);
				int age = rs.getInt(2);
				int refno = rs.getInt(3);
				String email = rs.getString(4);
				employeeDtoFromDB = new EmployeeDto(name, age, refno, email);
			}
			pStmt = connection.prepareStatement("update employee set name=?,age=?,refno=?,email=? where id=?");
			pStmt.setString(1, employeeDto.getName() != null ? employeeDto.getName() : employeeDtoFromDB.getName());
			pStmt.setInt(2, employeeDto.getAge() != 0 ? employeeDto.getAge() : employeeDtoFromDB.getAge());
			pStmt.setInt(3, employeeDto.getRefno() != 0 ? employeeDto.getRefno() : employeeDtoFromDB.getRefno());
			pStmt.setString(4, employeeDto.getEmail() != null ? employeeDto.getEmail() : employeeDtoFromDB.getEmail());
			pStmt.setInt(5, employeeId);
			boolean udpateStatus = pStmt.execute();
			System.out.println("Updation Status:" + udpateStatus);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void particularEmployeeDelete(int employeeId) {
		Connection connection = getConnection();
		PreparedStatement pStmt;
		try {
			pStmt = connection.prepareStatement("delete from employee where id=?");
			pStmt.setInt(1, employeeId);
			boolean deleteStatus = pStmt.execute();
			System.out.println("Deletion Status:" + deleteStatus);
		} catch (SQLException e) {
			e.printStackTrace();

		}
	}

	private Connection getConnection() {
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ms1_schema", "root", "root");
			return conn;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;

	}

}
