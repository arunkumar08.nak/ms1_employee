package com.apple.ms1.dto;

public class EmployeeDto {
	
	
	private String name;
	private int age;
	private int refno;
	private String email;
	
	public EmployeeDto(String name, int age, int refno, String email) {
		super();
		this.name = name;
		this.age = age;
		this.refno = refno;
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getRefno() {
		return refno;
	}
	public void setRefno(int refno) {
		this.refno = refno;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	

}
